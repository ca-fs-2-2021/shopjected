<?php

require 'bootstrap.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/main.css">
    <title>SHOP front page</title>
</head>
<body>
<div class="header">
    <h2>THE SHITTY SHOP</h2>
    <nav>
        <a href="admin/index.php">ADMIN ZONE</a>
        <a href="index.php">Url to nowhere</a>
    </nav>
</div>
<div class="container">
    <?php
    if (isset($_GET['disp']) && $_GET['disp'] === 'products') { require 'templates/products.php'; }
    elseif (isset($_GET['disp']) && $_GET['disp'] === 'product') { require 'templates/product.php'; }
    else { require 'templates/news.php'; }
    ?>

</div>
<div class="footer">
    FOOTER
</div>

</body>
</html>