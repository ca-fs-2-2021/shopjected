<?php

require 'bootstrap.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/main.css">
    <title>Admin page</title>
</head>
<body>
<div class="header">
    <nav>
        <a href="../index.php">SHITTY SHOP</a>
        <a href="index.php">Dashboard</a>
        <a href="index.php?disp=products">Products</a>
        <a href="index.php?disp=categories">Categories</a>
    </nav>
</div>
<div class="container">
    <?php
    if (isset($_GET['disp']) && $_GET['disp'] === 'products') { require 'templates/products.php'; }
    elseif (isset($_GET['disp']) && $_GET['disp'] === 'categories') { require 'templates/categories.php'; }
    else { require 'templates/dashboard.php'; }
    ?>

</div>
<div class="footer">
    FOOTER
</div>

</body>
</html>