<?php
if (isset($_POST) && !empty($_POST) && $_POST['action'] === 'add') addProductToDatabase($_POST);
if (isset($_POST) && !empty($_POST) && $_POST['action'] === 'update') updateProduct($_POST, $_GET['id']);
if (isset($_GET['action']) && !empty($_GET) && $_GET['action'] === 'delete') deleteProduct($_GET['id']);
if (isset($_GET['action']) && !empty($_GET) && $_GET['action'] === 'edit') $productData = getProduct($_GET['id']);
?>
<div class="content-box">
    <div class="add-box">
        <h1>Products</h1>
        <form method="post">
            <div class="form-control">
                <label for="name">Product name:</label>
                <input type="text" id="name" name="name" value="<?= $productData['name'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="description">Description:</label>
                <textarea id="description" name="description"
                          rows="3"><?= $productData['description'] ?? '' ?></textarea>
            </div>
            <div class="form-control">
                <label for="sku">SKU:</label>
                <input type="text" id="sku" name="sku" value="<?= $productData['sku'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="price">Price:</label>
                <input type="number" id="price" name="price" step="0.01" value="<?= $productData['price'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="special_price">Special price:</label>
                <input type="number" id="special_price" name="special_price" step="0.01"
                       value="<?= $productData['special_price'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="cost">Cost:</label>
                <input type="number" id="cost" name="cost" step="0.01" value="<?= $productData['cost'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="quantity">Quantity:</label>
                <input type="number" id="quantity" name="quantity" value="<?= $productData['quantity'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="category">Category:</label>
                <select name="category" id="category">
                          <?php foreach (getAllCategories() as $category): ?>
                        <option value="0" disabled><?= strtoupper($category['name']) ?></option>
                        <?php foreach (getSubCategories($category['id']) as $subCategory): ?>
                            <option value="<?= $subCategory['id'] ?>"><?= ($subCategory['name']) ?></option>
                        <?php endforeach; endforeach; ?>
                </select>
            </div>
            <?php
            if (isset($_GET['action']) == 'edit'): ?>
                <input type="hidden" name="action" value="update"/>
                <button>Update Product</button>
            <?php else: ?>
                <input type="hidden" name="action" value="add"/>
                <button>Add Product</button>
            <?php endif; ?>

        </form>
    </div>
    <div class="result-box">
        <h1>Products already added</h1>
        <?php
        //if (isset($_POST)) var_dump($_POST); //TODO DEL SAVES, reikes istrint
        showAllProductsList();
        ?>
    </div>
</div>