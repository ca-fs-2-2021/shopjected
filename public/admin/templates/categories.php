<?php
if (isset($_POST) && !empty($_POST) && $_POST['action'] === 'add') addCategoryToDatabase($_POST);
if (isset($_POST) && !empty($_POST) && $_POST['action'] === 'update') updateCategory($_POST, $_GET['id']);
if (isset($_GET['action']) && !empty($_GET) && $_GET['action'] === 'delete') deleteCategory($_GET['id']);
if (isset($_GET['action']) && !empty($_GET) && $_GET['action'] === 'edit') $categoryData = getCategory($_GET['id']);
?>
<div class="content-box">
    <div class="add-box">
        <h1>Categories</h1>
        <form method="post">
            <div class="form-control">
                <label for="name">Category name:</label>
                <input type="text" id="name" name="name" value="<?= $categoryData['name'] ?? '' ?>">
            </div>
            <div class="form-control">
                <label for="description">Description:</label>
                <textarea id="description" name="description"
                          rows="3"><?= $categoryData['description'] ?? '' ?></textarea>
            </div>
            <div class="form-control">
                <label for="parent_id">Parent Category:</label>
                <select name="parent_id" id="parent_id">
                    <option value="0">No parent</option>
                    <?php foreach (getAllCategories() as $category): ?>
                        <option value="<?= $category['id'] ?>"
                            <?php
                            if (isset($categoryData['parent_id']) && $categoryData['parent_id'] === $category['id']) {
                                echo 'selected';
                            }
                            ?>
                        >
                            <?= $category['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-actions">
                <?php
                if (isset($_GET['action']) == 'edit'): ?>
                    <input type="hidden" name="action" value="update"/>
                    <button>Update Category</button>
                <?php else: ?>
                    <input type="hidden" name="action" value="add"/>
                    <button>Add Category</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
    <div class="result-box">
        <h1>Categories already added</h1>
        <?php
        //if (isset($_POST)) var_dump($_POST);
        $categories = getAllCategories();
        if ($categories) {
            foreach ($categories as $key => $category): ?>
                <?php if (!$category['parent_id']): ?>
                    <p class="category-main"><?= $category['name'] ?>
                        <a class="text-blue" href="?disp=categories&action=edit&id=<?= $category['id'] ?>">Edit</a>
                        <a class="text-red" href="?disp=categories&action=delete&id=<?= $category['id'] ?>">Delete</a>
                    </p>
                    <?php foreach (getSubCategories($category['id']) as $parent): ?>
                        <span class="category-parent">- <?= $parent['name'] ?>
                <a class="text-blue" href="?disp=categories&action=edit&id=<?= $parent['id'] ?>">Edit</a>
                        <a class="text-red" href="?disp=categories&action=delete&id=<?= $parent['id'] ?>">Delete</a>
                </span><br/>
                    <?php endforeach; endif; endforeach;
        } else {
            echo 'No categories';
        } ?>
    </div>
</div>