<?php


/*
 * PRODUCTS FUNCTIONS
 */


function addProductToDatabase($product)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("INSERT INTO Products 
    (name, description, sku, price, special_price, cost, quantity)
    VALUES (:name, :description, :sku, :price, :special_price, :cost, :quantity)");
    $stmt->bindParam(':name', $product['name']);
    $stmt->bindParam(':description', $product['description']);
    $stmt->bindParam(':sku', $product['sku']);
    $stmt->bindParam(':price', $product['price']);
    $stmt->bindParam(':special_price', $product['special_price']);
    $stmt->bindParam(':cost', $product['cost']);
    $stmt->bindParam(':quantity', $product['quantity']);
    $stmt->execute();

    $productId = $pdo->lastInsertId();
    $stmt = $pdo->prepare("INSERT INTO Matches (product_id, category_id) VALUES (:product_id, :category_id)");
    $stmt->bindParam(':product_id', $productId);
    $stmt->bindParam(':category_id', $product['category']);
    $stmt->execute();

}
function updateProduct($product, $id)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("UPDATE Products SET 
                    name = :name, 
                    description = :description,
                    sku = :sku,
                    price = :price,
                    special_price = :special_price,
                    cost = :cost,
                    quantity = :quantity
                    WHERE id = $id");

    $stmt->bindParam(':name', $product['name']);
    $stmt->bindParam(':description', $product['description']);
    $stmt->bindParam(':sku', $product['sku']);
    $stmt->bindParam(':price', $product['price']);
    $stmt->bindParam(':special_price', $product['special_price']);
    $stmt->bindParam(':cost', $product['cost']);
    $stmt->bindParam(':quantity', $product['quantity']);
    $stmt->execute();

    header('Location: index.php?disp=products');
}

function showAllProductsList()
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Products");
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
        $i = 1;
        foreach ($result as $product) {
            echo $i . '. ' . $product['name'] . ' ';
            echo '<a class="text-red" href="index.php?disp=products&action=edit&id=' . $product['id'] . '">Edit</a> ';
            echo '<a class="text-blue" href="index.php?disp=products&action=delete&id=' . $product['id'] . '">Delete</a><br />';
            $i++;
        }
    } else {
        echo 'No products in database';
    }
}

function getProduct($id) {
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Products WHERE id = $id");
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function deleteProduct($id)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("DELETE FROM Products WHERE id = $id");
    $stmt->execute();
}

/*
 * CATEGORIES FUNCTIONS
 */

function addCategoryToDatabase($category)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("INSERT INTO Categories 
    (name, description, parent_id)
    VALUES (:name, :description, :parent_id)");
    $stmt->bindParam(':name', $category['name']);
    $stmt->bindParam(':description', $category['description']);
    $stmt->bindParam(':parent_id', $category['parent_id']);
    $stmt->execute();
}

function updateCategory($category, $id)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("UPDATE Categories SET 
                    name = :name, 
                    description = :description,
                    parent_id = :parent_id
                    WHERE id = $id");

    $stmt->bindParam(':name', $category['name']);
    $stmt->bindParam(':description', $category['description']);
    $stmt->bindParam(':parent_id', $category['parent_id']);
    $stmt->execute();

    header('Location: index.php?disp=categories');

}


function getAllCategories(): array
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Categories WHERE parent_id = 0");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getSubCategories($id): array
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Categories WHERE parent_id = $id");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);

}
function getSubCategory($id): array
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Categories WHERE parent_id = $id");
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);

}

function getCategory($id) {
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("SELECT * FROM Categories WHERE id = $id");
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function deleteCategory($id)
{
    global $config;
    $pdo = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("DELETE FROM Categories WHERE id = $id");
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM Categories WHERE parent_id = $id");
    $stmt->execute();
}
